# PCNTL demo - 23. 1. 2019

### How to build and run container:

```bash
docker build -t php74 .
docker run -dit -v $(pwd):/code php74
docker ps
docker exec -it <container_id> bash
```

### Testing
```
# In the first terminal
cd /code
php test.php

# In the second terminal
ps -a
kill <PID> # sends SIGTERM
```

### Supervisor
```
[program:my-worker]
command=php /home/worker/job --param=a,b,c
autostart=true
autorestart=true
stopsignal=TERM
numprocs=1
```

### Contact
```
Tomas Kozak
Slack Pehapkari - tomas.kozak
```
