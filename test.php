<?php

class Worker {

  private bool $running = false;

  public function start(): void {
    $this->running = true;
    while ($this->running) {
      $this->loop();
      pcntl_signal_dispatch();
    }
  }

  public function stop(): void {
    $this->running = false;
  }

  private function loop(): void {
    $this->job(1);
    $this->job(2);
    $this->job(3);
    $this->job(4);
    $this->job(5);
  }

  private function job(int $i) :void {
    echo (string)$i . "\n";
    sleep(1);
  }
}

$worker = new Worker();
pcntl_signal(SIGTERM, function($c,$i) use ($worker) {
  $worker->stop();
});
$worker->start();
