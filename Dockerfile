FROM debian:stretch-slim

RUN apt-get update && apt-get install -y \
		ca-certificates \
		curl \
		xz-utils \
		autoconf \
		dpkg-dev \
		file \
		g++ \
		gcc \
		libc-dev \
		make \
		pkg-config \
		re2c \
		git \
		bison \
		libcurl4-openssl-dev \
		libedit-dev \
		libsodium-dev \
		libsqlite3-dev \
		libssl-dev \
		libxml2-dev \
		zlib1g-dev \
		procps \
		nano

RUN mkdir -p /usr/src/php
WORKDIR /usr/src/php
RUN git clone https://github.com/php/php-src.git php-src
WORKDIR /usr/src/php/php-src

RUN ./buildconf && ./configure \
	--enable-pcntl \
	--enable-maintainer-zts \
	--enable-debug \
	--enable-cli \
	--with-curl \
	--with-libedit \
	--with-openssl \
	--with-zlib \
	--with-mhash \
	--enable-ftp \
	--enable-mbstring \
	--enable-mysqlnd

RUN make -j5 && make install

ENTRYPOINT ["php"]
